package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import parqueadero.Carro;
import parqueadero.Central;

public class Main 
{
	private BufferedReader br;
	private Central central;

	/**
	 * Clase principal de la aplicaci�nn, incializa el mundo.
	 * @throws Exception
	 */
	public Main() throws Exception
	{

		br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("** PLATAFORMA CITY PARKING S.A.S **");
		System.out.println();
		central = new Central ();
		menuInicial();
	}
	
	/**
	 * Menú principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Men� principal: \n"
			+ "1. Registrar Cliente \n"
			+ "2. Parquear Siguente Carro En Cola \n"
			+ "3. Atender Cliente Que Sale \n"
	        + "4. Ver Estado Parqueaderos \n"
	        + "5. Ver Carros En Cola \n"
			+ "6. Salir \n\n"
			+ "Opcion: ";

		boolean terminar = false;
		while ( !terminar )
		{
			System.out.print(mensaje);
			int op1 = Integer.parseInt(br.readLine());
			while (op1>6 || op1<1)
			{
				System.out.println("\nERROR: Ingrese una opci�n valida\n");
				System.out.print(mensaje);
				op1 = Integer.parseInt(br.readLine());
			}

			switch(op1)
			{
				case 1: registrarCliente(); break;
				case 2: parquearCarro(); break;
				case 3: salidaCliente(); break;
				case 4: verEstadoParquederos(); break;
				case 5: verCarrosEnCola(); break;
				case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
			}
		}

	}

  /**
   * M�todo para registrar a un nuevo cliente
   * @throws Exception
   */
	public void registrarCliente () throws Exception
  {
	  System.out.println("** REGISTRAR CLIENTE **");
	  System.out.println("Ingrese el color del carro");
	  String color = br.readLine();
	  System.out.println("Ingrese la matricula");
	  String matricula = br.readLine();
	  System.out.println("Ingrese el nombre del conductor");
	  String nombre = br.readLine();
	  
	  central.registrarCliente(color, matricula, nombre);
	  
  }
  /**
   * M�todo para parquear un carro que se encuentra en la cola
   * @throws Exception
   */
  public void parquearCarro() throws Exception
  {
	  System.out.println("** PARQUEAR CARRO EN COLA **");
	  try{
	  String respuesta = central.parquearCarroEnCola();
	  System.out.println(respuesta);
	  }
	  catch(Exception e){
		  System.out.println(e.getMessage());
	  }
  }
 /**
  * M�todo para sacar un carro del parqueadero
  * @throws Exception
  */
  public void salidaCliente () throws Exception
  {
	  System.out.println("** REGISTRAR SALIDA CLIENTE **");
	  System.out.println("Ingrese la matricula del auto que va a salir");
	  String matricula = br.readLine();
	  try{
		  double precio = central.atenderClienteSale(matricula);
		  System.out.println("El costo de su estadía es de " + precio + " pesos.");
	  }
	  catch(Exception e){
		  System.out.println(e.getMessage());
	  }
	  
  }
  /**
   * M�todo que permite visualizar graficaente el estado de los parqueaderos
   * @throws Exception
   */
  public void verEstadoParquederos() throws Exception
  {
	  System.out.println("** ESTADO PARQUEADEROS **");
	 System.out.println();
	 System.out.println();
	 System.out.println("__________________________________________________________");
	 System.out.println(central.estadoPila1());
	 System.out.println("__________________________________________________________");
	 System.out.println(central.estadoPila2());
	 System.out.println("__________________________________________________________");
	 System.out.println(central.estadoPila3());
	 System.out.println("__________________________________________________________");
	 System.out.println(central.estadoPila4());
	 System.out.println("__________________________________________________________");
	 System.out.println(central.estadoPila5());
	 System.out.println("__________________________________________________________");
	 System.out.println(central.estadoPila6());
	 System.out.println("__________________________________________________________");
	 System.out.println(central.estadoPila7());
	 System.out.println("__________________________________________________________");
	 System.out.println(central.estadoPila8());
	 System.out.println("__________________________________________________________");
	  
  }
  
  /**
   * M�todo que permite visualizar graficaente el estado de la cola de carros pendientes por parquear
   * @throws Exception
   */
  public void verCarrosEnCola () throws Exception
  {
	  System.out.println("** ESTADO COLA **");
	  System.out.println();
	  System.out.println();
	  System.out.println("__________________________________________________________");
	  System.out.println(central.estadoCola());
	  System.out.println("__________________________________________________________");
	  System.out.println();
  }
  
  /**
	 * Main...
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new Main();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
