package parqueadero;

import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;


public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	private Queue <Carro> colaCarros;
	
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	private Stack <Carro> pila1;
	private Stack <Carro> pila2;
	private Stack <Carro> pila3;
	private Stack <Carro> pila4;
	private Stack <Carro> pila5;
	private Stack <Carro> pila6;
	private Stack <Carro> pila7;
	private Stack <Carro> pila8;

    
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	private Stack <Carro> pilaTemporal;
   
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
    	pila1 = new Stack();
    	pila2 = new Stack();
    	pila3 = new Stack();
    	pila4 = new Stack();
    	pila5 = new Stack();
    	pila6 = new Stack();
    	pila7 = new Stack();
    	pila8 = new Stack();
    	
    	pilaTemporal = new Stack();
    	
    	colaCarros = new Queue();
        
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	String col = pColor;
    	String matricula = pMatricula;
    	String nombre = pNombreConductor;
    	
    	Carro aRegistrar = new Carro(col, matricula, nombre);
    	
    	colaCarros.enqueue(aRegistrar);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	Carro aParquear = colaCarros.dequeue();
    	if(aParquear == null){
    		throw new Exception ("No hay carros en la cola");
    	}
    	else{
    		String mat = aParquear.darMatricula();
    		int pila = 0;
    		int pos = 0;
    		boolean seParqueo = false;
    		if(pila1.getSize() < 4){
    			pila = 1;
    			pila1.push(aParquear);
    			seParqueo = true;
    			pos = pila1.getSize();
    		}
    		else if(pila2.getSize() < 4){
    			pila = 2;
    			pila2.push(aParquear);
    			seParqueo = true;
    			pos = pila2.getSize();
    		}
    		else if(pila3.getSize() < 4){
    			pila = 3;
    			pila3.push(aParquear);
    			seParqueo = true;
    			pos = pila3.getSize();
    		}
    		else if(pila4.getSize() < 4){
    			pila = 4;
    			pila4.push(aParquear);
    			seParqueo = true;
    			pos = pila4.getSize();
    		}
    		else if(pila5.getSize() < 4){
    			pila = 5;
    			pila5.push(aParquear);
    			seParqueo = true;
    			pos = pila5.getSize();
    		}
    		else if(pila6.getSize() < 4){
    			pila = 6;
    			pila6.push(aParquear);
    			seParqueo = true;
    			pos = pila6.getSize();
    		}
    		else if(pila7.getSize() < 4){
    			pila = 7;
    			pila7.push(aParquear);
    			seParqueo = true;
    			pos = pila7.getSize();
    		}
    		else if(pila8.getSize() < 4){
    			pila = 8;
    			pila8.push(aParquear);
    			seParqueo = true;
    			pos = pila8.getSize();
    		}
    		if(seParqueo == false){
    			throw new Exception ("El parqueadero está lleno, no se pudo parquear.");
    		}
    		else{
    			return ("El carro con matricula " + mat + " se ha parqueado en la pila " + pila + " posicion " + pos );
    		}
    	}
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	Carro aSalir = sacarCarro(matricula);
    	if(aSalir == null){
    		throw new Exception ("No se encuentra un carro con la matricula dada");
    	}
    	double precio = cobrarTarifa(aSalir);
    	return precio;
    	
    	
    	
    }
    
  /**
   * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	return "PARQUEADERO X";    	    
    }
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {
    	if(buscarEnPila(pila1, matricula) == true){
    		return sacarCarroP1(matricula);
    	}
    	else if (buscarEnPila(pila2, matricula) == true){
    		return sacarCarroP2(matricula);
    	}
    	else if (buscarEnPila(pila3, matricula) == true){
    		return sacarCarroP3(matricula);
    	}
    	else if (buscarEnPila(pila4, matricula) == true){
    		return sacarCarroP4(matricula);
    	}
    	else if (buscarEnPila(pila5, matricula) == true){
    		return sacarCarroP5(matricula);
    	}
    	else if (buscarEnPila(pila6, matricula) == true){
    		return sacarCarroP6(matricula);
    	}
    	else if (buscarEnPila(pila7, matricula) == true){
    		return sacarCarroP7(matricula);
    	}
    	else if (buscarEnPila(pila8, matricula) == true){
    		return sacarCarroP8(matricula);
    	}
    	
    	return null;

    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	Carro car = null;
    	while((car = pila1.pop()) != null){
    		if(matricula.equals(car.darMatricula())){
    			break;
    		}
    			pilaTemporal.push(car);
    	}
    	Carro seQueda = null;
    	while((seQueda = pilaTemporal.pop()) != null){
    		pila1.push(seQueda);
    	}
    	return car;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	Carro car = null;
    	while((car = pila2.pop()) != null){
    		if(matricula.equals(car.darMatricula())){
    			break;
    		}
    			pilaTemporal.push(car);
    	}
    	Carro seQueda = null;
    	while((seQueda = pilaTemporal.pop()) != null){
    		pila2.push(seQueda);
    	}
    	return car;
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	Carro car = null;
    	while((car = pila3.pop()) != null){
    		if(matricula.equals(car.darMatricula())){
    			break;
    		}
    			pilaTemporal.push(car);
    	}
    	Carro seQueda = null;
    	while((seQueda = pilaTemporal.pop()) != null){
    		pila3.push(seQueda);
    	}
    	return car;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	
    	Carro car = null;
    	while((car = pila4.pop()) != null){
    		if(matricula.equals(car.darMatricula())){
    			break;
    		}
    			pilaTemporal.push(car);
    	}
    	Carro seQueda = null;
    	while((seQueda = pilaTemporal.pop()) != null){
    		pila4.push(seQueda);
    	}
    	return car;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	Carro car = null;
    	while((car = pila5.pop()) != null){
    		if(matricula.equals(car.darMatricula())){
    			break;
    		}
    			pilaTemporal.push(car);
    	}
    	Carro seQueda = null;
    	while((seQueda = pilaTemporal.pop()) != null){
    		pila5.push(seQueda);
    	}
    	return car;
    }
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	
    	Carro car = null;
    	while((car = pila6.pop()) != null){
    		if(matricula.equals(car.darMatricula())){
    			break;
    		}
    			pilaTemporal.push(car);
    	}
    	Carro seQueda = null;
    	while((seQueda = pilaTemporal.pop()) != null){
    		pila6.push(seQueda);
    	}
    	return car;
    }
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	
    	Carro car = null;
    	while((car = pila7.pop()) != null){
    		if(matricula.equals(car.darMatricula())){
    			break;
    		}
    			pilaTemporal.push(car);
    	}
    	Carro seQueda = null;
    	while((seQueda = pilaTemporal.pop()) != null){
    		pila7.push(seQueda);
    	}
    	return car;
    }
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	
    	Carro car = null;
    	while((car = pila8.pop()) != null){
    		if(matricula.equals(car.darMatricula())){
    			break;
    		}
    			pilaTemporal.push(car);
    	}
    	Carro seQueda = null;
    	while((seQueda = pilaTemporal.pop()) != null){
    		pila8.push(seQueda);
    	}
    	return car;
    }
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	Date fecha = new Date();
    	long millis2 = fecha.getTime();
    	long millis1 = car.darLlegada();
    	long resta = millis2 - millis1;
    	long minutos = TimeUnit.MILLISECONDS.toMinutes(resta);
    	
    	double respuesta = minutos * 25;
    	return respuesta;	
    }
    
    /**
     * Retorna si se encuentra el carro con la matricula dada en la pila indicada.
     */
    public boolean buscarEnPila(Stack <Carro> pPila, String pMatricula){
    	Carro item = null;
    	Stack <Carro> temp = new Stack();
    	boolean encontrado = false;
    	while(( item =  pPila.pop()) != null){
    		temp.push(item);
    		if(item.darMatricula().equals(pMatricula)){
    			encontrado = true;
    			break;
    		}
    	}
    	while((item = temp.pop()) != null){
    		pPila.push(item);
    	}
    	return encontrado;
    }
    
    public String estadoPila1(){
    	int numCarros = pila1.getSize();
    	int espaciosLibres = 4 - numCarros;
    	String respuesta = "P1   ";
    	for(int i = 0; i < espaciosLibres; i++){
    		respuesta = respuesta + "|-Libre-|";
    	}
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|Ocupado|";
    	}
    	return respuesta;
    	
    }
    public String estadoPila2(){
    	int numCarros = pila2.getSize();
    	int espaciosLibres = 4 - numCarros;
    	String respuesta = "P2   ";
    	for(int i = 0; i < espaciosLibres; i++){
    		respuesta = respuesta + "|-Libre-|";
    	}
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|Ocupado|";
    	}
    	return respuesta;
    	
    }
    public String estadoPila3(){
    	int numCarros = pila3.getSize();
    	int espaciosLibres = 4 - numCarros;
    	String respuesta = "P3   ";
    	for(int i = 0; i < espaciosLibres; i++){
    		respuesta = respuesta + "|-Libre-|";
    	}
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|Ocupado|";
    	}
    	return respuesta;
    	
    }
    public String estadoPila4(){
    	int numCarros = pila4.getSize();
    	int espaciosLibres = 4 - numCarros;
    	String respuesta = "P4   ";
    	for(int i = 0; i < espaciosLibres; i++){
    		respuesta = respuesta + "|-Libre-|";
    	}
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|Ocupado|";
    	}
    	return respuesta;
    	
    }
    public String estadoPila5(){
    	int numCarros = pila5.getSize();
    	int espaciosLibres = 4 - numCarros;
    	String respuesta = "P5   ";
    	for(int i = 0; i < espaciosLibres; i++){
    		respuesta = respuesta + "|-Libre-|";
    	}
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|Ocupado|";
    	}
    	return respuesta;
    	
    }
    public String estadoPila6(){
    	int numCarros = pila6.getSize();
    	int espaciosLibres = 4 - numCarros;
    	String respuesta = "P6   ";
    	for(int i = 0; i < espaciosLibres; i++){
    		respuesta = respuesta + "|-Libre-|";
    	}
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|Ocupado|";
    	}
    	return respuesta;
    	
    }
    public String estadoPila7(){
    	int numCarros = pila7.getSize();
    	int espaciosLibres = 4 - numCarros;
    	String respuesta = "P7   ";
    	for(int i = 0; i < espaciosLibres; i++){
    		respuesta = respuesta + "|-Libre-|";
    	}
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|Ocupado|";
    	}
    	return respuesta;
    	
    }
    public String estadoPila8(){
    	int numCarros = pila8.getSize();
    	int espaciosLibres = 4 - numCarros;
    	String respuesta = "P8   ";
    	for(int i = 0; i < espaciosLibres; i++){
    		respuesta = respuesta + "|-Libre-|";
    	}
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|Ocupado|";
    	}
    	return respuesta;
    	
    }
    
    public String estadoCola(){
    	int numCarros = colaCarros.getSize();
    	String respuesta = "COLA   ";
    	for(int i = 0; i < numCarros; i++){
    		respuesta = respuesta + "|En Espera|";
    	}
    	return respuesta;
    }
}
